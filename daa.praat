# Creates 10 sound samples of the syllable [dʌ:]

for i from 1 to 10

	Create KlattGrid: "daa", 0, 0.5, 3, 0, 0, 0, 0, 0, 0
	Add pitch point: 0.05, 190
	Add pitch point: 0.55, 150
	Add voicing amplitude point: 0.05, 0
	Add voicing amplitude point: 0.05001, 90
	Add voicing amplitude point: 0.45, 90
	Add voicing amplitude point: 0.5, 0


	f1_locus = 800 - (400/9) * (i-1)
	f2_locus = 2000 - (500/9) * (i-1)

	Add oral formant frequency point: 1, 0, f1_locus
	Add oral formant frequency point: 2, 0, f2_locus
	Add oral formant frequency point: 3, 0, 2600

	Add oral formant frequency point: 1, 0.05, f1_locus
	Add oral formant frequency point: 2, 0.05, f2_locus
	Add oral formant frequency point: 3, 0.05, 2600

	Add oral formant frequency point: 1, 0.1, 850
	Add oral formant frequency point: 2, 0.1, 1200
	Add oral formant frequency point: 3, 0.1, 2800

	Add oral formant bandwidth point: 1, 0.1, 90
	Add oral formant bandwidth point: 2, 0.1, 90
	Add oral formant bandwidth point: 3, 0.1, 90

	Add oral formant bandwidth point: 1, 0, 90
	Add oral formant bandwidth point: 2, 0, 90
	Add oral formant bandwidth point: 3, 0, 90

	To Sound
	selectObject: "KlattGrid daa"
	Remove

endfor